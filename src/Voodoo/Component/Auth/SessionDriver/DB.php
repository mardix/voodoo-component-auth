<?php
/**
 * DB
 * 
 * @name DB
 * @author Mardix
 * @since   Feb 13, 2014
 * 
 * Save the session in a relational DB
 * 
 */

namespace Voodoo\Component\Auth\SessionDriver;

use Voodoo,
    Voodoo\Component\Auth;

class DB extends Auth\Base
{
    use TDriver;
    
    protected $tableName = "user_session";
    
    protected static $session = null;

    protected $config = null;
    
    protected function setup()
    {
        parent::setup();
        $this->config = Voodoo\Core\Config::Component()->get("Auth");
    }
    
    /**
     * Create a new Session
     * 
     * @param int $userId
     * @param int $ttl - Manually set the TTL
     * @param bool $shadow_session - Creates another session with the same user_id, but can't be removed by the non shadow  
     * @return bool
     */
    public function createNew($userId, $ttl = null, $shadow_session = false)
    {
        if (! $shadow_session) {
            $this->reset()->where(["user_id" => $userId, "shadow_session" => 0])->delete();
        }
        
        $sessionId = $this->createSessionId();
        $expireTime = time() + ($ttl ?: $this->config["sessionTTL"]);
        $ins = $this->insert([
            "user_id" => $userId,
            "session_id" => $sessionId,
            "ip" => Voodoo\Core\Http\Request::getIp(),
            "expired_at" => date("Y-m-d H:i:s", $expireTime),
            "shadow_session" => $shadow_session ? 1 : 0
        ]);
        $this->setCookie($sessionId, $expireTime);
        return $ins;
    }
    
    /**
     * Return a live Session entity
     * 
     * @return Session
     */
    public function getSession()
    {
        if (! self::$session) {
            $session = $this->reset()
                    ->where([
                        "session_id" => $this->getCookie(),
                        "expired_at > ? " => $this->NOW()
                     ])->findOne();
            if ($session) {
                
                $liveSessionExpiredAt = $session->live_session_expired_at;
                if (strtotime($liveSessionExpiredAt) < time()) {
                    $ttl = time() + $this->config["liveSessionTTL"];
                    #$session->update(["live_session_expired_at" => $ttl]);
                }
                self::$session = $session;
            }
        }
        return self::$session;
    }
    
    /**
     * Return the count live sessions
     * @param bool $all - When true, it will count all session
     * @return int
     */
    public function getCount($all = false)
    {
        if ($all) {
            return $this->reset()->count("id");
        } else {
            return $this->reset()
                    ->where(["live_session_expired_at > ?" => $this->NOW()])
                    ->count(("id"));
        }
    }
    
    /**
     * Destroy a session
     * 
     * @return bool
     */
    public function destroy()
    {
        $session = $this->getSession();
        if ($session) {
            $this->delete();      
            $this->setCookie("", 0);
            return true;            
        }
        return false;
    }    

    /**
     * return the current session entry
     * 
     * @return self
     */
    public function getUser()
    {
        $session = $this->getSession();
        if ($session) {
            return (new Auth\User)->findOne($session->user_id);
        }
        return false;
      
    }    

    /**
     * Drop all session
     * 
     * @param bool $all - if true, it will destroy all, otherwise it will destroy expired one only
     * @return boolean
     */
    public function destroyAll($all=false)
    {
        if ($all) {
            $this->reset()->delete(true);
        } else {
            $this->reset()
                    ->whereLt("expired_at", $this->NOW())
                    ->delete();            
        }
        return true;
    }
    
    
    /**
     * To reset the cookie TTL
     * 
     * @return type
     */
    public function resetTTL($ttl=null)
    {
        $session = $this->getSession();
        if ($session) {
            $expireTime = time() + ($ttl ?: $this->config["sessionTTL"]);
            $session->update([
                "ip" => Voodoo\Core\Http\Request::getIp(),
                "expired_at" => date("Y-m-d H:i:s", $expireTime)                
            ]);
            $this->setCookie($session->session_id, $expireTime);
        }        
    }
    
    /**
     * Save data in local storage
     * @param string $data
     */
    protected function saveData($data)
    {
        $session = $this->getSession();
        if ($session) {
          $session->update(["data" => $data]);
        }        
    }
    
    /**
     * Retrieve data from local storage
     * @return string
     */
    protected function retrieveData()
    {
        $session = $this->getSession();
        if ($session) {
          return $session->data;
        }  
        return null;
    }

    
/*******************************************************************************/
    protected function setupTable()
    {
        $sql = "
            CREATE TABLE `{$this->getTableName()}` (
                `id` INT(10) NOT NULL AUTO_INCREMENT,
                `user_id` INT(10) NULL,
                `session_id` CHAR(45) NOT NULL,
                `ip` VARCHAR(250) NULL,
                `data` MEDIUMTEXT NULL,
                `shadow_session` TINYINT(1) NOT NULL DEFAULT '0',
                `expired_at` DATETIME NULL,
                `live_session_expired_at` DATETIME NULL,
                `created_at` DATETIME NOT NULL,
                `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                INDEX `session_id` (`session_id`),
                INDEX `user_id` (`user_id`)
            )
        "; 
        $this->createTable($sql);
    }    
}
