<?php
/** Voodoo\Component
 ******************************************************************************
 * @desc        Login adds login functionalities to your application. Just extends it to your model
 * @package     Voodoo\Component\Auth
 * @name        Login
 * @copyright   (c) 2013
 ******************************************************************************/

namespace Voodoo\Component\Auth;

use Voodoo\Core,
    Voodoo\Component\Crypto;

class User extends Base
{
    const ACCESS_LEVEL_USER = 0;
    const ACCESS_LEVEL_VIEWER = 1;
    const ACCESS_LEVEL_EDITOR = 2;
    const ACCESS_LEVEL_MANAGER = 3;
    const ACCESS_LEVEL_ADMIN = 4;
    const ACCESS_LEVEL_SUPERADMIN = 5;
    
    CONST OAUTH_FACEBOOK = "facebook";
    CONST OAUTH_TWITTER = "twitter";
    CONST OAUTH_GOOGLE = "google";
    CONST OAUTH_GITHUB = "github";
    CONST OAUTH_AMAZON = "amazon";
    CONST OAUTH_YAHOO = "yahoo";  
    
    protected $tableName = "user";
    
    protected $randomPasswordLen = 8;
    
    public function findByEmail($email)
    {
        if(! Core\Helpers::validEmail($email)) {
            throw new Exception("Invalid email");
        }
        return $this->reset()->where(["email" => $email])->findOne();        
    }
    
    public function findByOAuth($authProvider, $uid)
    {
        $column = "{$authProvider}_uid";
        return $this->reset()->where([$column => $uid])->findOne();
    }
    
    
    /**
     * To login with email and password
     *
     * @param type $login
     * @param type $password
     * @return type
     * @throws Exception
     */
    public function loginWithEmail($email, $password)
    {
        $user = $this->findByEmail($email);
        if ($user) {
            $password = trim($password);
            if (! $user->passwordMatch($password)) {
                throw new Exception("Invalid password");
            }
            $user->updateLastLogin();
            return $user;
        } else {
            throw new Exception("Invalid Login");
        }
    }
    
    /**
     * Login with OAUTH
     * 
     * @param string $authProvider
     * @param int $uid
     * @return \Voodoo\Component\Auth\User
     * @throws Exception
     */
    public function loginWithOAuth($authProvider, $uid)
    {
        $user = $this->findByOAuth($authProvider, $uid);
        if (! $user) {
            throw new Exception("Invalid Login");
        }
        $user->updateLastLogin();
        return $user;
    }
    
    /**
     * create new Account with email address / pw
     *
     * @param type $email
     * @param type $password
     * @throws Exception
     */
    public function createWithEmail($email, $password = null, $screen_name = "")
    {
        if(! Core\Helpers::validEmail($email)) {
            throw new Exception("Invalid Email");
        }
        if($password && ! Core\Helpers::validPassword($password)) {
            throw new Exception("Invalid password");
        }

        $email = $this->formatEmail($email);
        $screen_name = trim(strip_tags($screen_name));
        if(! $this->emailExists($email)) {
            $password = trim($password);
            $hash = (new Crypto\Password)->hash(trim($password));
            return $this->insert([
                        "screen_name" => $screen_name,
                        "email" => $email,
                        "password_hash" => $hash,
                        "display" => 1
                    ]);
        } else {
            throw new Exception("Email exists already");
        }
    }
    
    /**
     * create new account with OAuth
     * 
     * @param string $authProvider
     * @param int $uid
     * @return \Voodoo\Component\Auth\User
     * @throws Exception
     */
    public function createWithOAuth($authProvider, $uid)
    {
        $column = "{$authProvider}_uid";
        $user = $this->reset()->where([$column => $uid])->findOne();
        if ($user) {
            throw new Exception("Login exists already");
        }
        return $this->insert([
            $column => $uid,
            "display" => 1,
            "has_auth_login" => 1
        ]);
    }
    
    /**
     * Update the last login datetime
     * 
     * @return \Voodoo\Component\Auth\Login
     */
    public function updateLastLogin()
    {
        if ($this->isSingleRow()) {
            $this->update(["last_login" => $this->NOW()]);
        }
        return $this;
    }
    
    /**
     * To reset a password and return the newly generated one
     * @param string $email
     * @return string
     * @throws Exception
     */
    public function resetPassword() 
    {
        $password = strtolower(trim(Core\Helpers::generateRandomString($this->randomPasswordLen)));
        $this->update([
            "password_hash" => (new Crypto\Password)->hash($password)
        ]);
        $this->setRequirePasswordChange(true);
        return $password;
    }


    /**
     * Change the password
     *
     * @param type $password
     * @return \App\Www\Adminzone\Model\Admin\User
     * @throws Exception
     */
    public function changePassword($password)
    {
        if(! Core\Helpers::validPassword($password)) {
            throw new Exception("Invalid password");
        } else {
            $password = trim($password);
            $hash = (new Crypto\Password)->hash($password);
            $this->update(["password_hash" => $hash]);
            
            if ($this->requirePasswordChange()) {
                $this->setRequirePasswordChange(false);
            }
        }
        return $this;
    }

    /**
     * Change the login email
     * 
     * @param type $email
     * @return \App\Www\Adminzone\Model\Admin\User
     * @throws Exception
     */
    public function changeEmail($email)
    {
        if(! Core\Helpers::validEmail($email)) {
            throw new Exception("Invalid email");
        } else {
            $email = $this->formatEmail($email);
            if(! $this->emailExists($email)) {
                $this->update(["email" => $email]);
            } else {
                throw new Exception("Email exists already");
            }
        }
        return $this;
    }

    /**
     * Get the email
     * @return string
     */
    public function getEmail()
    {
        return $this->email ;
    }
    
     /**
     * Set the screen name
     * @param string $creen_name
     * @return \Voodoo\Component\Auth\User
     */
    public function setScreenName($screen_name)
    {
        $this->update(["screen_name" => $screen_name]);
        return $this;
    }
    
    /**
     * Get the screen name
     * @return string
     */
    public function getScreenName()
    {
        $column = "screen_name";
        return $this->$column ;
    }   
    
    /**
     * Set the auth token and secret
     * @param type $authProvider
     * @param type $token
     * @param type $secret
     * @return \Voodoo\Component\Auth\User
     */
    public function setOAuthToken($authProvider, $token, $secret = "") {
        $this->update([
            "{$authProvider}_token" => $token,   
            "{$authProvider}_token_secret" => $secret            
        ]);
        return $this;
    }
    
    /**
     * Retun the token 
     * 
     * @param string $authProvider
     * @return Array[token, token_secret]
     */
    public function getOAuthToken($authProvider)
    {
        $column_token = $authProvider . "_token";
        $column_secret = $authProvider . "_token_secret";
        return [
            "token" => $this->$column_token,
            "token_secret" => $this->$column_secret
        ];
    }
    
    /**
     * Set the OAUTH 
     * @param type $authProvider
     * @param type $uid
     * @return \Voodoo\Component\Auth\User
     */
    public function setOAuthUID($authProvider, $uid)
    {
        $this->update(["{$authProvider}_uid" => $uid]);
        return $this;
    }
    
    /**
     * Get the AUTH 
     * @param string $authProvider
     * @return string
     */
    public function getOAuthUID($authProvider)
    {
        $column = $authProvider . "_uid";
        return $this->$column ;
    }
    
    /**
     * Set the default OAUTH provider 
     * @param string $provider
     * @return \Voodoo\Component\Auth\User
     */
    public function setDefaultOAuthProvider($provider)
    {
        $this->update(["default_oauth_provider" => $provider]);
        return $this;
    }
    
    /**
     * Get the default OAuth Provider
     * @return string
     */
    public function getDefaultOAuthProvider()
    {
        return $this->default_oauth_provider ;
    }
    
    /**
     * 
     * @param type $authProvider
     * @param string $creen_name
     * @return \Voodoo\Component\Auth\User
     */
    public function setOAuthTwitterScreenName($screen_name)
    {
        $this->update(["twitter_screen_name" => $screen_name]);
        return $this;
    }
    
    /**
     * Get the AUTH 
     * @return string
     */
    public function getOAuthTwitterScreenName()
    {
        $column = "twitter_screen_name";
        return $this->$column ;
    }
    
    
    /**
     * Check if the OAUTH for a certain provider is set by checking the  
     * Or if the account has auth enabled
     * 
     * @param string $authProvider
     * @return string
     */
    public function hasOAuth($authProvider = null)
    {
        if (!$authProvider){
            return ($this->has_auth_login) ? true : false;
        } else {
            $column = $authProvider . "_uid";
            return ($this->$column) ? true : false  ;           
        }

    }
      
    /**
     * Check if email exists
     *
     * @param type $email
     * @return type
     * @throws Exception
     */
    public function emailExists($email)
    {
        $email = $this->formatEmail($email);
        if(Core\Helpers::validEmail($email)) {
            $user = $this->reset()->where(["email" => $email])->count("id");
            return ($user) ? true : false;
        } else {
            throw new Exception("Invalid email");
        }
    }

    /**
     * Check if a password match the current password
     * 
     * @param string $password
     * @return bool
     */
    public function passwordMatch($password)
    {
        return (new Crypto\Password)->verify(trim($password), $this->password_hash);   
    }
    
    /**
     * Set the require password on the account
     * 
     * @param bool $bool
     * @return \Voodoo\Component\Auth\User
     */
    public function setRequirePasswordChange($bool = true)
    {
        $this->update(["require_password_change" => $bool ? 1 : 0]);
        return $this;
    }
    
    /**
     * Check if a password change is required
     * 
     * @return bool
     */
    public function requirePasswordChange()
    {
        return $this->require_password_change ? true : false;
    }
    
    /**
     * Top opt-in/out user from a newsletter
     * @param bool $bool
     * @return \Voodoo\Component\Auth\User
     */
    public function setNewsletterOptin($bool = true)
    {
        $this->update(["newsletter_optin" => $bool ? 1 : 0]);
        return $this;
    }
    
    /**
     * Check if user is optin/out of newletter
     * @return bool
     */
    public function optinNewsletter()
    {
        return $this->newsletter_optin ? true : false;
    }
    
    /**
     * Set the profile photo
     * @param string $photo
     * @return \Voodoo\Component\Auth\User
     */
    public function setProfilePhoto($photo)
    {
        $this->update(["profile_photo" => $photo]);
        return $this;
    }
    
    /**
     * Get profile photo
     * 
     * @return string
     */
    public function getProfilePhoto()
    {
        return $this->profile_photo;
    }
    
    /**
     * Set the access level
     * @param int $acl
     * @return \Voodoo\Component\Auth\User
     */
    public function setAccessLevel($acl)
    {
        $this->update(["access_level" => $acl]);
        return $this;
    }
    
    /**
     * Get the access level
     * @return int
     */
    public function getAccessLevel()
    {
        return $this->access_level;
    }
    
    /**
     * Prepare the email to be processed
     *
     * @param type $email
     * @return string
     */
    private function formatEmail($email)
    {
        return trim(strtolower($email));
    }
    
/*******************************************************************************/
    protected function setupTable()
    {
        $sql = "
        CREATE TABLE `{$this->getTableName()}` (
            `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `screen_name` VARCHAR(50) NOT NULL DEFAULT '',
            `email` VARCHAR(125) NOT NULL DEFAULT '',
            `password_hash` VARCHAR(125) NOT NULL DEFAULT '',
            `profile_photo` VARCHAR(125) NOT NULL DEFAULT '',
            `access_level` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
            `display` TINYINT(1) NOT NULL DEFAULT '1',
            `newsletter_optin` TINYINT(1) NOT NULL DEFAULT '1',
            `last_login` DATETIME NOT NULL,
            `require_password_change` TINYINT(1) NOT NULL DEFAULT '0',
            `created_at` DATETIME NOT NULL,
            `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `has_auth_login` TINYINT(1) NOT NULL DEFAULT '0',
            `default_oauth_provider` VARCHAR(50) NOT NULL DEFAULT '',
            `facebook_uid` VARCHAR(50) NULL DEFAULT NULL,
            `facebook_token` VARCHAR(125) NULL DEFAULT NULL,
            `facebook_token_secret` VARCHAR(250) NULL DEFAULT NULL,
            `twitter_uid` VARCHAR(50) NULL DEFAULT NULL,
            `twitter_token` VARCHAR(50) NULL DEFAULT NULL,
            `twitter_token_secret` VARCHAR(250) NULL DEFAULT NULL,
            `twitter_screen_name` VARCHAR(50) NULL DEFAULT NULL,
            `google_uid` VARCHAR(50) NULL DEFAULT NULL,
            `google_token` VARCHAR(50) NULL DEFAULT NULL,
            `google_token_secret` VARCHAR(250) NULL DEFAULT NULL,
            `github_uid` VARCHAR(50) NULL DEFAULT NULL,
            `github_token` VARCHAR(50) NULL DEFAULT NULL,
            `github_token_secret` VARCHAR(250) NULL DEFAULT NULL,

            PRIMARY KEY (`id`),
            INDEX `email` (`email`),
            INDEX `display` (`display`),
            INDEX `facebook_uid` (`facebook_uid`),
            INDEX `twitter_uid` (`twitter_uid`),
            INDEX `google_uid` (`google_uid`),
            INDEX `github_uid` (`github_uid`)
        )    
        ";
        $this->createTable($sql);
    }
   
}

