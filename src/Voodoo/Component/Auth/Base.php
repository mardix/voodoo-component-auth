<?php

/**
 * Base
 * 
 * @name Base
 * @author Mardix
 * @since   Feb 12, 2014
 */

namespace Voodoo\Component\Auth;

use Voodoo,
    PDO;

abstract class Base extends Voodoo\Component\Model\BaseModel
{
    public function __construct(PDO $pdo = null)
    {
        $this->dbAlias = Voodoo\Core\Config::Component()->get("Auth.dbAlias");
        parent::__construct($pdo);
    }    
}
