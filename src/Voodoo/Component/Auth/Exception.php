<?php
/** Voodoo\Component
 ******************************************************************************
 * @desc        Exception
 * @package     Voodoo\Component\Auth
 * @name        Exception
 * @copyright   (c) 2013
 ******************************************************************************/
namespace Voodoo\Component\Auth;

class Exception extends \Exception
{
    
}
