
#Voodoo\Component\Auth

A component to create mailing list, invite list, and newsletter

##Setup

Add the code below in the file: /App/_conf/Component.conf.php

    [Auth]
        dbAlias = "MyTest"
        sessionTTL = 2592000 ; Session time to live. 30 days online
        sessionDriver = "\Voodoo\Component\Auth\SessionDriver\DB" ; DB|Redis or full namespace
        sessionName = "c_" ; Session cookie name
        redisDbAlias = "MyRedisTest" ; The DB alias for Redis Session
        liveSessionTTL = 300 ; Live session TTL

#Example

### Create new Account

    try {
        $user = (new Auth\User)->createWithEmail($email, $password);
    } catch (Auth\Exception as $ae) {
        
    } catch (Exception as $e) {

    } 

###Start a session

    try {
        $login = (new Auth\User)->loginWithEmail($email, $password);
        if ($login) {
            (new Auth\Session)->createNew($login->user_id);
        }
    } catch (Auth\Exception as $ae) {
        
    } catch (Exception as $e) {

    }

###To access session user
    
    $user = (new Auth\Session)->getUser();
    if ($user) {
        echo $user->getScreenName();
    }

$liveSession = (new Auth\Session)->getCount();